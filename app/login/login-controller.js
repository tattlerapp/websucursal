app.controller("LoginController", function($scope, $http, auth){

  $scope.originalUser= angular.copy($scope.user);
  var route = $scope.$root.server + "accounts/login";

  $scope.resetUserForm = function(isOk){
    $scope.successfull = isOk;
    $scope.showMessage = true;
    $scope.user = angular.copy($scope.originalUser);
    $scope.userForm.$setPristine();
    $scope.submitProgress = false;
    $scope.showValidation = false;
  }

  $scope.UserLogin = function(userDetails){
		if(!$scope.userForm.$invalid){
	  		$scope.submitProgress = true;
			$http({
			    method: "post",
			    url: route,
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			    data: $.param({username: userDetails.email, password: userDetails.password, client_id: "tattlerWebApp", grant_type: "password"})
			}).
	      	success(function(data,status) {
	        	$scope.resetUserForm(true);
	        	auth.login(data.username, data.access_token, data.chat_session.token);
	        	$scope.$root.showMenu=true;
	    	}).
	      	error(function(data,status){
	        	$scope.resetUserForm(false);
        	});
		} else{
			$scope.showValidation = true;
		}
	}

	$scope.getError = function (error) {
		if(angular.isDefined(error)){
			if(error.required){
				return "Por favor, introduzca un valor.";
			} else if(error.email){
				return "Por favor, introduzca un Email v\u00e1lido";
			} else if(error.pattern){
				return "Por favor, introduzca un valor v\u00e1lido";
			} else if(error.minlength){
				return "El password debe ser mayor a 6 caracteres";
			}
		}
	}
});
