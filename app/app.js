var app = angular.module("tattlerApp",
        [   "ngRoute",
            "routeStyles",
            "chieffancypants.loadingBar",
            "ngAnimate",
            "multi-select",
            "ngCookies",
            "ui.grid",
            "ui.grid.edit",
            "ui.grid.cellNav",
            "ui.grid.selection",
            "ngAutocomplete",
            "ngMap",
            "mgcrea.ngStrap",
            "ngImgCrop"
        ]
    )
    .config(function($routeProvider){
        $routeProvider
            .when("/login", {
                templateUrl: "login/login.html",
                css: "login/login.css",
                controller: "LoginController"
            })
            .when("/dashboard", {
                templateUrl: "dashboard/dashboard.html",
                css: "dashboard/dashboard.css",
                controller: "DashboardController"
            })
            .when("/chat", {
              templateUrl: "chat/chat.html",
              css: "chat/chat.css",
              controller: "ChatController"
            })
            .otherwise({
                redirectTo:"/login"
            });
    })
    .directive("0000000000000000", function(){
        return {
            link: function postLink(scope, element, attrs){
                scope.$on("$routeChangeSuccess", function(){
                    $(window).scrollTop(0);
                });

                scope.$on("$viewContentLoaded", function(){
                    $(".carousel").carousel();
                });
            }
        };
    });

app.factory("auth", function($rootScope,$cookies,$cookieStore,$location)
{
    return{
        login : function(username, token, chat_token)
        {
            //creamos la cookie con el nombre que nos han pasado
            $cookies.username = username;
            $cookies.token = token;
            $cookies.chat_token = chat_token;
            //mandamos a la home
            $location.path("/home");
            $rootScope.showMenu=true;
        },
        logout : function()
        {
            //al hacer logout eliminamos la cookie con $cookieStore.remove
            $cookieStore.remove("username"),
            $cookieStore.remove("token");
            $cookieStore.remove("chat_token");
            //mandamos al login
            $location.path("/login");
            $rootScope.showMenu=false;
        },
        checkStatus : function()
        {
            //creamos un array con las rutas que queremos controlar
            var rutasPrivadas = ["/dashboard","/promotion","/chat"];
            if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.username) == "undefined")
            {
                $location.path("/login");
            }
            //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
            else if($location.path() == "/login" && typeof($cookies.username) != "undefined")
            {
                $location.path("/dashboard");
                $rootScope.showMenu=true;
                $rootScope.username=$cookies.username;
            }
            //en el caso de que intente acceder a la ruta privada y ya haya iniciado sesión lo mandamos a la home
            else if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.username) != "undefined")
            {
                $rootScope.username=$cookies.username;
                $rootScope.showMenu=true;

                switch($location.path()) {
                  case "/dashboard":
                     $rootScope.activated = 1;
                     $rootScope.titulo = "Panel Sucursal"
                     $rootScope.icono = "fa fa-dashboard"
               }
            }
        },
        in_array : function(needle, haystack)
        {
            var key = '';
            for(key in haystack)
            {
                if(haystack[key] == needle)
                {
                    return true;
                }
            }
            return false;
        }
    }
});

app.run(function($rootScope, auth){
    $rootScope.server = "http://ec2-54-69-1-179.us-west-2.compute.amazonaws.com/api/";
    //$rootScope.server = "\u0068\u0074\u0074\u0070\u003a\u002f\u002f\u0065\u0063\u0032\u002d\u0035\u0034\u002d\u0036\u0039\u002d\u0031\u0034\u0033\u002d\u0034\u0039\u002e\u0075\u0073\u002d\u0077\u0065\u0073\u0074\u002d\u0032\u002e\u0063\u006f\u006d\u0070\u0075\u0074\u0065\u002e\u0061\u006d\u0061\u007a\u006f\u006e\u0061\u0077\u0073\u002e\u0063\u006f\u006d\u003a\u0038\u0030\u002f";
    //al cambiar de rutas
    $rootScope.$on('$routeChangeStart', function()
    {
        //llamamos a checkStatus, el cual lo hemos definido en la factoria auth
        //la cuál hemos inyectado en la acción run de la aplicación
        auth.checkStatus();
    });

    $rootScope.logout = function()
    {
        auth.logout();
    }
});

app.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue;
            }
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('ngTap', function() {
    return function(scope, element, attrs) {
      var tapping;
      tapping = false;
      element.bind('touchstart', function(e) {
        element.addClass('active');
        tapping = true;
      });
      element.bind('touchmove', function(e) {
        element.removeClass('active');
        tapping = false;
      });
      element.bind('touchend', function(e) {
        element.removeClass('active');
        if (tapping) {
          scope.$apply(attrs['ngTap'], element);
        }
      });
    };
  });
